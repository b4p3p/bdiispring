package bdiishared.services.interfaces;

import org.springframework.security.core.Authentication;

public interface ILoginService {

    public Authentication Login(String username, String password);

    public boolean IsLogged(Authentication auth);

}
