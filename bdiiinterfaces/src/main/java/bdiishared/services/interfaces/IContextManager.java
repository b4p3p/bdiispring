package bdiishared.services.interfaces;

import bdiishared.model.classes.User;
import org.springframework.security.core.Authentication;

import java.util.List;

/**
 * Created by b4p3p on 05/02/14.
 */
public interface IContextManager {

    public List<User> GetAllUsers(Authentication auth);

}
