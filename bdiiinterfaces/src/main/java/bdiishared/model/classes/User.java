package bdiishared.model.classes;

import bdiishared.model.interfaces.IUser;

import java.io.Serializable;
import java.rmi.Remote;

public class User implements Serializable, Remote, IUser {

    private String username;
    private String password;
    private int level;

    public User(String username, String password, int level) {
        this.username = username;
        this.password = password;
        this.level = level;
    }

    public String GetUsername() {
        return username;
    }

    public String GetPassword() {
        return password;
    }

    public int GetLevel() {
        return level;
    }

}
