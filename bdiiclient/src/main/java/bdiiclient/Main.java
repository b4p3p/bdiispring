package bdiiclient;

import bdiiclient.controller.ContextManager;
import bdiiclient.log.LogManager;
import bdiiclient.view.controller.ViewManager;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {

        int err = 0;

        try {
            ContextManager.Init();
        } catch (Exception e) {
            LogManager.Error("Start application", e);
            err = 1;
        }

        ViewManager.Init(primaryStage);
        ViewManager.GoToLogin(err);
    }

    public static void main(String[] args) {
        launch(args);
    }
}

/*
ApplicationContext ctx = new ClassPathXmlApplicationContext("/spring/application-context.xml");
ILoginService loginService = (ILoginService) ctx.getBean("loginService");
IContextManager contextManager = (IContextManager) ctx.getBean("contextManager");

Authentication auth = loginService.Login("asd", "asd");
System.out.println("isLogged: " + loginService.IsLogged(auth));

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
*/