package bdiiclient.util;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Created by b4p3p on 06/02/14.
 */
public class ImageLoader {

    public static ImageView getEditImageView() {
        ImageView imageView = new ImageView(getEditImage());
        imageView.setFitWidth(25);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        return imageView;
    }

    public static Image getEditImage() {
        Image img = null;
        img = new Image("/images/command/edit.png");
        return img;
    }

    public static ImageView getDeleteImage() {
        Image img = null;
        img = new Image("/images/command/delete.png");
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(25);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        return imageView;
    }

}
