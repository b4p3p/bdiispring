package bdiiclient.view.controller;

import bdiiclient.Main;
import bdiiclient.log.LogManager;
import bdiiclient.view.controller.record.RisLoadNode;
import bdiiclient.view.controller.record.RisLoadParent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class ViewManager {
    private static Stage primaryStage;

    public static void Init(Stage stage) {
        primaryStage = stage;
    }

    public static RisLoadNode LoadNode(String url) {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();
            Node node = (Node) fxmlLoader.load(Main.class.getResource(url).openStream());
            IController controller = fxmlLoader.getController();

            return new RisLoadNode(node, controller);

        } catch (IOException ex) {
            LogManager.Error("CreateScene", ex);
            return null;
        }
    }

    public static RisLoadParent LoadParent(String url) {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();
            Parent node = fxmlLoader.load(Main.class.getResource(url));
            IController controller = fxmlLoader.getController();

            return new RisLoadParent(node, controller);

        } catch (IOException ex) {
            LogManager.Error("CreateScene", ex);
            return null;
        }
    }

    public static void ClearContent(VBox container) {
        container.getChildren().clear();
    }

    public static void AddInContent(VBox container, Node nodo) {
        container.getChildren().add(nodo);
    }

    public static void GoToLogin(int err) {

        RisLoadParent risLoadParent = LoadParent("/fxml/Login.fxml");
        Scene scene = new Scene(risLoadParent.parent);

        if (scene == null) return;

        primaryStage.setTitle("bdii project login");
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();

        if (err == 1) {
            LoginController loginController = (LoginController) risLoadParent.controller;
            loginController.SetMessageError("Connessione rifiutata");
            loginController.SetVisibilityMessageError(true);
        }

        primaryStage.show();
    }

    public static void GoToMain() {

        RisLoadParent risLoadNode = LoadParent("/fxml/Main.fxml");
        Scene scene = new Scene(risLoadNode.parent);

        if (scene == null) return;

        ClearMainContainer(scene);

        primaryStage.setTitle("bdii project");
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    public static void ClearMainContainer(Scene scene) {
        VBox container = (VBox) scene.lookup("#container");
        container.getChildren().clear();
    }

}
