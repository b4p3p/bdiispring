package bdiiclient.view.controller.main;

import bdiishared.model.classes.User;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.List;

public class ObservableUser {

    private final SimpleStringProperty username;
    private final SimpleStringProperty password;
    private final SimpleStringProperty level;

    public ObservableUser(User user) {
        this.username = new SimpleStringProperty(user.GetUsername());
        this.password = new SimpleStringProperty(user.GetPassword());
        this.level = new SimpleStringProperty(String.valueOf(user.GetLevel()));
    }

    public static ObservableList<ObservableUser> ToObservableList(List<User> listaUtenti) {
        ObservableList<ObservableUser> ris = FXCollections.observableArrayList();
        for (User u : listaUtenti) {
            ris.add(new ObservableUser(u));
        }
        return ris;
    }

    public String getUsername() {
        return username.get();
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getLevel() {
        return level.get();
    }

    public void setLevel(String level) {
        this.level.set(level);
    }

    public ImageView GetUserImage() {
        Image img = null;
        if (getLevel().equals("-1")) img = new Image("/images/typeUsers/guest.png");
        if (getLevel().equals("1")) img = new Image("/images/typeUsers/user.png");
        if (getLevel().equals("0")) img = new Image("/images/typeUsers/admin.png");
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(25);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        return imageView;
    }

}
