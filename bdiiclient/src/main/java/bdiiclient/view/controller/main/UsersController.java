package bdiiclient.view.controller.main;

import b4p3p.control.ImageButton;
import bdiiclient.controller.ContextManager;
import bdiiclient.util.ImageLoader;
import bdiiclient.view.controller.IController;
import bdiishared.model.classes.User;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import javafx.util.Pair;

import java.util.List;

/**
 * Created by b4p3p on 05/02/14.
 */
public class UsersController implements IController {

    @FXML
    public TableView lstUsers;
    @FXML
    public TableColumn columnLevel;
    @FXML
    public TableColumn columnEdit;
    @FXML
    public TableColumn columnDelete;

    public void LoadUsers() {

        List<User> users = ContextManager.GetUsers();
        ObservableList<ObservableUser> observableUsers = ObservableUser.ToObservableList(users);

        columnLevel.setCellValueFactory(new PairValueFactory());
        columnLevel.setCellFactory(new Callback<TableColumn<Pair<String, Object>, Object>, TableCell<Pair<String, Object>, Object>>() {
            @Override
            public TableCell<Pair<String, Object>, Object> call(TableColumn<Pair<String, Object>, Object> column) {
                return new CellaTypeUser();
            }
        });

        columnEdit.setCellValueFactory(new PairValueFactory());
        columnEdit.setCellFactory(new Callback<TableColumn<Pair<String, Object>, Object>, TableCell<Pair<String, Object>, Object>>() {
            @Override
            public TableCell<Pair<String, Object>, Object> call(TableColumn<Pair<String, Object>, Object> column) {
                return new CellaModifica();
            }
        });

        columnDelete.setCellValueFactory(new PairValueFactory());
        columnDelete.setCellFactory(new Callback<TableColumn<Pair<String, Object>, Object>, TableCell<Pair<String, Object>, Object>>() {
            @Override
            public TableCell<Pair<String, Object>, Object> call(TableColumn<Pair<String, Object>, Object> column) {
                return new CellaDelete();
            }
        });

        lstUsers.setItems(observableUsers);

    }

    public void cmdNuovoClick(ActionEvent actionEvent) {

    }
}

class CellaDelete extends TableCell<Pair<String, Object>, Object> {
    @Override
    protected void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            ImageButton btn = new ImageButton(ImageLoader.getDeleteImage());
            setGraphic(btn);
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    //TODO elimina utente
                    System.out.println("click!");
                }
            });
            setAlignment(Pos.CENTER);
        }
    }
}

class CellaModifica extends TableCell<Pair<String, Object>, Object> {
    @Override
    protected void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            ImageButton btn = new ImageButton(ImageLoader.getEditImageView());
            setGraphic(btn);
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    //TODO modifica utente
                    System.out.println("click!");
                }
            });
            setAlignment(Pos.CENTER);
        }
    }
}

class CellaTypeUser extends TableCell<Pair<String, Object>, Object> {
    @Override
    protected void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            ObservableUser os = (ObservableUser) item;
            setGraphic(os.GetUserImage());
            setText(null);
            setAlignment(Pos.CENTER);
        }
    }
}

/* senza questa classe non viene visualizzato nessun valore */
class PairValueFactory implements Callback<TableColumn.CellDataFeatures<Pair<String, Object>, Object>, ObservableValue<Object>> {
    @SuppressWarnings("unchecked")
    @Override
    public ObservableValue<Object> call(TableColumn.CellDataFeatures<Pair<String, Object>, Object> data) {
        Object value = data.getValue();
        return (value instanceof ObservableValue)
                ? (ObservableValue) value
                : new ReadOnlyObjectWrapper<Object>(value);
    }
}

















/*
class CellaTypeUser extends TableCell<Pair<String, Object>, Object> {

    @Override
    protected void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);

        if (item != null) {
            if (item instanceof String) {
                setText((String) item);
                setGraphic(null);
            } else if (item instanceof Integer) {
                setText(Integer.toString((Integer) item));
                setGraphic(null);
            } else if (item instanceof Boolean) {
                CheckBox checkBox = new CheckBox();
                checkBox.setSelected( (Boolean)item );
                setGraphic(checkBox);
            } else if (item instanceof Image) {
                setText(null);
                ImageView imageView = new ImageView( (Image) item);
                imageView.setFitWidth(100);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                setGraphic(imageView);
            } else if (item instanceof ObservableUser) {

                ObservableUser os = (ObservableUser)item;

                Image img = null;
                if ( os.getLevel().equals("-1")) img = new Image("/images/typeUsers/guest.png");
                if ( os.getLevel().equals("1")) img = new Image("/images/typeUsers/user.png");
                if ( os.getLevel().equals("0")) img = new Image("/images/typeUsers/admin.png");

                ImageView imageView = new ImageView( img );
                imageView.setFitWidth(25);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                setGraphic(imageView);

            }
            else
            {
                setText("N/A");
                setGraphic(null);
            }
        } else {
            setText(null);
            setGraphic(null);
        }
    }
}
*/
