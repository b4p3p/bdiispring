package bdiiclient.view.controller;

import bdiiclient.view.controller.main.UsersController;
import bdiiclient.view.controller.record.RisLoadNode;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class MainController implements IController {

    @FXML
    HBox hboxMenuBar;
    @FXML
    VBox container;

    private void DeselezionaTutto() {

        ObservableList<Node> children = hboxMenuBar.getChildren();
        for (Node n : children) {
            StackPane s = (StackPane) n;
            s.getStyleClass().clear();
            s.getStyleClass().add("itemNonSelezionato");
        }
    }

    private void SelezionaItem(String id) {

        DeselezionaTutto();

        StackPane rec = (StackPane) hboxMenuBar.lookup(id);
        rec.getStyleClass().clear();
        rec.getStyleClass().add("itemSelezionato");
    }

    /* BUTTON CLICK */

    public void cmdUserClick(ActionEvent actionEvent) {

        ViewManager.ClearContent(container);
        SelezionaItem("#stkCmdUsers");

        RisLoadNode rec = ViewManager.LoadNode("/fxml/main/frmUsers.fxml");

        ViewManager.AddInContent(container, rec.nodo);

        UsersController ctrl = (UsersController) rec.controller;
        ctrl.LoadUsers();

    }

    public void cmdDBClick(ActionEvent actionEvent) {

        ViewManager.ClearContent(container);
        SelezionaItem("#stkCmdDB");

    }

    /* END BUTTON CLICK */

}
