package bdiiclient.view.controller;

import bdiiclient.controller.ContextManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.security.core.Authentication;

public class LoginController implements IController {

    @FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Label lblAuthError;

    public LoginController() {

    }

    public void SetMessageError(String message) {
        lblAuthError.setText(message);
    }

    public void SetVisibilityMessageError(boolean value) {
        lblAuthError.setVisible(value);
    }

    public void cmdLoginClick(ActionEvent actionEvent) {

        Authentication auth = ContextManager.Login(txtUsername.getText(), txtPassword.getText());

        if (auth != null) {
            ViewManager.GoToMain();
            lblAuthError.setVisible(false);
        } else {
            lblAuthError.setVisible(true);
        }
        txtPassword.setText("");

    }

}
