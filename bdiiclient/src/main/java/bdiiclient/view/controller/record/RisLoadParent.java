package bdiiclient.view.controller.record;

import bdiiclient.view.controller.IController;
import javafx.scene.Parent;

public class RisLoadParent {

    public Parent parent;
    public IController controller;

    public RisLoadParent(Parent node, IController controller) {
        this.parent = node;
        this.controller = controller;
    }

}
