package bdiiclient.view.controller.record;

import bdiiclient.view.controller.IController;
import javafx.scene.Node;

/**
 * @author b4p3p
 */
public class RisLoadNode {

    public Node nodo;
    public IController controller;

    public RisLoadNode(Node node, IController controller) {
        this.nodo = node;
        this.controller = controller;
    }

}
