package bdiiclient.controller;

import bdiishared.model.classes.User;
import bdiishared.services.interfaces.IContextManager;
import bdiishared.services.interfaces.ILoginService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.core.Authentication;

import java.util.List;

/**
 * Created by b4p3p on 05/02/14.
 */
public class ContextManager {

    private static ApplicationContext appContext;
    private static ILoginService loginService;
    private static IContextManager contextManager;
    private static Authentication authentication;
    private static boolean FLAG_LOCK = true;

    public static void Init() throws Exception {

        appContext = new ClassPathXmlApplicationContext("/spring/application-context.xml");
        loginService = (ILoginService) appContext.getBean("loginService");
        contextManager = (IContextManager) appContext.getBean("contextManager");
        FLAG_LOCK = false;
    }

    public static Authentication Login(String username, String password) {

        if (FLAG_LOCK) return null;

        authentication = loginService.Login(username, password);
        return authentication;
    }

    public static List<User> GetUsers() {

        return contextManager.GetAllUsers(authentication);
    }
}
