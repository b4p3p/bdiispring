package bdiiserver.db;

/**
 * Created by b4p3p on 05/02/14.
 */
public class TableSchema {

    public final static String USERS = "users";

    public static final String USER_USERNAME = "username";
    public static final String USER_PASSWORD = "password";
    public static final String USER_LEVEL = "level";

}
