package bdiiserver.services;

import bdiiserver.controller.properties.AppProperties;
import bdiiserver.controller.properties.SimpleAutenticationManager;
import bdiiserver.log.LogManager;
import bdiishared.model.interfaces.IUser;
import bdiishared.services.interfaces.ILoginService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.Serializable;
import java.rmi.Remote;

public class LoginService implements ILoginService, Remote, Serializable {

    /*
    private AppProperties GetUserSession()
    {
        //ApplicationContext context = new AnnotationConfigApplicationContext(AppProperties.class);
        //AppProperties session = (AppProperties) context.getBean("propertiesBean");
        //return AppProperties.authManager;
    }
    */

    @Override
    public Authentication Login(String username, String password) {

        IUser user = ContextManager.GetUser(username, password);

        if (user != null) {
            LogManager.Info("connessione: " + user.GetUsername());
            ContextManager.GetUser(username, password);

            //TODO aggiungere il tipo dell'utente

            SimpleAutenticationManager am = AppProperties.authManager;
            Authentication request = new UsernamePasswordAuthenticationToken(username, password);
            Authentication result = am.authenticate(request,
                    SimpleAutenticationManager.TYPE_AUTHORITIES.admin);
            SecurityContextHolder.getContext().setAuthentication(result);

            return result;
        } else {
            return null;
        }

    }

    @Override
    public boolean IsLogged(Authentication auth) {

        String msg = AppProperties.messaggio;
        return auth.isAuthenticated();

    }
}
