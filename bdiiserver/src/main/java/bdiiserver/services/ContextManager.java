package bdiiserver.services;

import bdiiserver.controller.DBConverter;
import bdiiserver.db.TableSchema;
import bdiiserver.log.LogManager;
import bdiishared.model.classes.User;
import bdiishared.model.interfaces.IUser;
import bdiishared.services.interfaces.IContextManager;
import bdiishared.services.interfaces.ILoginService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;

import java.io.Serializable;
import java.rmi.Remote;
import java.sql.*;
import java.util.List;

@Scope(value = "session")
public class ContextManager implements IContextManager, Remote, Serializable {

    private ApplicationContext ctx = null;
    private ILoginService loginService = null;

    private static String USERNAME = "bdii";
    private static String PASSWORD = "bdii";

    private static Connection Connect() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Connection conn = null;
        conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bdii", USERNAME, PASSWORD);
        return conn;
    }

    private static void CloseConnection(PreparedStatement statement, Connection conn) {
        try {
            statement.close();
            conn.close();
        } catch (SQLException ex) {
            LogManager.Error("CloseConnection", ex);
        }
    }

    public static IUser GetUser(String username, String password) {

        try {
            Connection conn = Connect();

            String sql = String.format("SELECT * FROM %s WHERE %s=? and %s=?",
                    TableSchema.USERS, TableSchema.USER_USERNAME, TableSchema.USER_PASSWORD);

            PreparedStatement prepareStatement = conn.prepareStatement(sql);
            prepareStatement.setString(1, username);
            prepareStatement.setString(2, password);
            ResultSet executeQuery = prepareStatement.executeQuery();

            executeQuery.next();

            User user = DBConverter.ToUser(executeQuery);

            CloseConnection(prepareStatement, conn);

            return user;

        } catch (SQLException ex) {
            LogManager.Error("GetError", ex);
            return null;

        } catch (ClassNotFoundException e) {
            LogManager.Error("GetError", e);
            return null;
        }
    }

    @Override
    public List<User> GetAllUsers(Authentication auth) {

        try {
            Connection conn = ContextManager.Connect();

            String sql = String.format("SELECT * FROM %s", TableSchema.USERS);
            PreparedStatement prepareStatement = conn.prepareStatement(sql);
            ResultSet executeQuery = prepareStatement.executeQuery();
            List<User> listaUtenti = DBConverter.ToListUser(executeQuery);

            ContextManager.CloseConnection(prepareStatement, conn);

            return listaUtenti;

        } catch (SQLException | ClassNotFoundException e) {
            LogManager.Error("GetError", e);
        }
        return null;

    }
}
