package bdiiserver.controller.properties;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public class SimpleAutenticationManager {

    //static final List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();

    public static enum TYPE_AUTHORITIES {
        admin,
        user
    }

    /*
    static {
        AUTHORITIES.add( new SimpleGrantedAuthority("ROLE_USER") );
    }
    */

    public Authentication authenticate(Authentication auth, TYPE_AUTHORITIES type) throws AuthenticationException {

        List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();

        switch (type) {
            case admin:
                AUTHORITIES.add(new SimpleGrantedAuthority("admin"));
            case user:
                AUTHORITIES.add(new SimpleGrantedAuthority("user"));
        }

        if (auth.getName().equals(auth.getCredentials())) {
            return new UsernamePasswordAuthenticationToken(
                    auth.getName(),
                    auth.getCredentials(), AUTHORITIES);
        }
        throw new BadCredentialsException("Bad Credentials");
    }

}
