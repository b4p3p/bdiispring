package bdiiserver.controller;

import bdiiserver.db.TableSchema;
import bdiiserver.log.LogManager;
import bdiishared.model.classes.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by b4p3p on 05/02/14.
 */
public class DBConverter {

    public static User ToUser(ResultSet executeQuery) {
        try {

            String username = executeQuery.getString(TableSchema.USER_USERNAME).trim();
            String password = executeQuery.getString(TableSchema.USER_PASSWORD).trim();
            Integer level = executeQuery.getInt(TableSchema.USER_LEVEL);

            return new User(username, password, level);

        } catch (SQLException e) {
            LogManager.Error("DBConverter.Touser ", e);
            return null;
        }
    }

    public static ArrayList<User> ToListUser(ResultSet executeQuery) {

        try {
            ArrayList<User> listaUtenti = new ArrayList<>();

            while (executeQuery.next()) {
                listaUtenti.add(ToUser(executeQuery));
            }

            return listaUtenti;

        } catch (Exception e) {
            LogManager.Error("DBConverter.ToListUser", e);
            return new ArrayList<>();
        }
    }
}
